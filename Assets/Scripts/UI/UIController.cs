﻿/*
Author: Stefan Stegic
Date: 28.4.2019.
Description: 
- Draws necessary crap on screen such as the selection rectangle.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class UIController : Singleton<UIController>
{
    // Variables for the selection rectangle
    private Vector2 rect1, rect2;
    [SerializeField]
    private float smallestSize;
    [SerializeField]
    private SelectionRect rectInstance;

    private void Update() {
        if (Input.GetMouseButton(0)) LBDown(Input.mousePosition);
        
        if (Input.GetMouseButtonUp(0)) LBUp(Input.mousePosition);

        if (Input.GetMouseButtonDown(1)) RBPress(Input.mousePosition);

        if (Input.GetMouseButtonDown(0)) LBPress(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.S)) SPress();
    }

    private void SPress()
    {
        foreach (Selectable unit in SelectionRect.Instance.selected)
        {
            unit.GetComponent<UnitBase>().StopAll();
        }
    }
    private void RBPress(Vector2 position)
    {
        // Convert to world position
        position = Camera.main.ScreenToWorldPoint(position);
        
        foreach (Selectable unit in SelectionRect.Instance.selected)
        {
            unit.GetComponent<UnitBase>().MoveTo(position);
        }
    }
    private void LBDown(Vector2 position)
    {
        if(!rectInstance.gameObject.active)
        {
            // Set the starting rectangle diagonal edge
            rect1 = position;

            // Clear previous selection
            rectInstance.ClearSelection();
        }
        
        // Update the second diagonal edge while the mouse button is down
        rect2 = position;

        float size = rect2.x - rect1.x;
        if(Mathf.Abs(size) < smallestSize) rect2.x = rect1.x + Mathf.Sign(size)*smallestSize;
        size = rect2.y - rect1.y;
        if(Mathf.Abs(size) < smallestSize) rect2.y = rect1.y + Mathf.Sign(size)*smallestSize;

        // DRAW THE RECTANGLE
        // Transform points into world space
        Vector2 point1 = Camera.main.ScreenToWorldPoint(rect1);
        Vector2 point2 = Camera.main.ScreenToWorldPoint(rect2);

        // Activate rect instance
        rectInstance.gameObject.SetActive(true);

        // Scale it and move it
        float xscale, yscale;
        xscale = (point2.x - point1.x) / rectInstance.baseWidth;
        yscale = -(point2.y - point1.y) / rectInstance.baseHeight;
        rectInstance.transform.localScale = new Vector3(xscale, yscale, 1);

        rectInstance.transform.SetPositionAndRotation(point1, Quaternion.identity);

    }

    private void LBPress(Vector2 mousePosition)
    {
        // Debug toggle cell emptiness
        if (Input.GetKey(KeyCode.LeftControl))
        {
            Vector2 point = Camera.main.ScreenToWorldPoint(mousePosition);
            Grid.Cell cell = Grid.Instance.CellAtPoint(point.x, point.y);
            cell.empty = !cell.empty;
        }
    }

    private void LBUp(Vector2 position)
    {
        rectInstance.gameObject.SetActive(false);
    }
}
