﻿/*

Author: Stefan Stegic
Date: 23.4.2019.
Description: Every unit must derive from this base class.
    Includes selection, movement and commands/actions etc.

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Priority_Queue;

public abstract class UnitBase : MonoBehaviour
{
    [SerializeField]
    private Transform selectedCircle;

    private float selectedAnimationDuration = 0.15f;

    public string unitName;

    private Vector3 targetPosition;
    private float moveSpeed = 2f;
    private float targetTolerance = 0.1f;

    private Rigidbody2D rb;
    private bool selected = false;
    private List<Vector3> movePath;
    protected void _baseAwake() {
        rb = GetComponent<Rigidbody2D>();
        rb.freezeRotation = true;
        targetPosition = transform.position;

        movePath = new List<Vector3>();
        // Listen to the select events
        Selectable sel = GetComponent<Selectable>();
        sel.OnSelect += OnSelected;
        sel.OnDeselect += OnDeselected;
    }

    public void OnSelected()
    {
        selected = true;
    }

    public void OnDeselected()
    {
        selected = false;
    }

    protected void _baseUpdate()
    {
        // Move towards targetPosition
        Vector2 difference = targetPosition - transform.position;
        if (difference.magnitude > targetTolerance)
        {
            rb.velocity = difference.normalized * moveSpeed;
        }
        else
        {
            if (movePath.Count == 0)
            {
                // Done moving, stop
                rb.velocity = Vector3.zero;
            }
            else
            {
                // Grab next move point
                targetPosition = movePath[0];
                movePath.RemoveAt(0);
            }
        }

        // Grow or shrink selected circle
        if (selected)
        {
            selectedCircle.gameObject.SetActive(true);
            // Grow to full
            if (selectedCircle.localScale.x < 0.4f)
                selectedCircle.transform.localScale += Vector3.one * 0.05f;
        } 
        else 
        {
            // Shrink to min
            if (selectedCircle.localScale.x > 0)
            {
                selectedCircle.transform.localScale -= Vector3.one * 0.05f;
            }
            else
            {
                selectedCircle.gameObject.SetActive(false);
            }
        }
    }

    protected void DirectMoveTo(Vector3 position)
    {
        targetPosition = position;
    }

    public void MoveTo(Vector3 position)
    {
        // Calculate shortest route to position and enter moving state
        movePath = FindPath(position);
        if (movePath.Count > 0)
        {
            targetPosition = movePath[0];
            movePath.RemoveAt(0);
        }
    }

    public void StopAll()
    {
        // Stop doing everything and enter idle state
        movePath.Clear();
        targetPosition = transform.position;
    }

    private float OctileDistance(Vector2 point1, Vector2 point2)
    {
        float min = Mathf.Abs(point2.x - point1.x);
        float max = Mathf.Abs(point2.y - point1.y);
        if (min > max)
        {
            float temp = min;
            min = max;
            max = temp;
        }

        return 1.4142135f * min + (max - min);
    }

    private class SearchCell
    {
        public float g, h;
        public Grid.Cell gridCell;
        public SearchCell parent;
        private static int[] directions = { 0, 1, // North
                                            1, 0, // East
                                            0, -1, // South
                                            -1, 0, // West
                                            1, 1, // North-east
                                            1, -1, // South-east
                                            -1, -1, // South-west
                                            -1, 1 // North-west
                                            };
        public SearchCell(float pastSteps, float heuristic, Grid.Cell gridCell, SearchCell parent)
        {
            this.g = pastSteps;
            this.h = heuristic;
            this.gridCell = gridCell;
            this.parent = parent;
        }

        public float f()
        {
            return g + h;
        }
        public int CompareTo(SearchCell other)
        {
            if (g + h < other.g + other.h) return -1;
            if (g + h > other.g + other.h) return 1;
            return 0;
        }
        public List<Grid.Cell> GetNeighbours()
        {
            List<Grid.Cell> ret = new List<Grid.Cell>();
            // Examine all eight neighbouring cells
            Grid.Cell current = null;
            int x, y;
            //string print = "Neighbours of " + gridCell.x + ", " + gridCell.y + ": ";
            
            for (int dirIndex=0; dirIndex < directions.Length; dirIndex += 2)
            {
                x = gridCell.x + directions[dirIndex];
                y = gridCell.y + directions[dirIndex+1];

                //print += string.Format("({0}, {1})", x, y);
                current = Grid.Instance.CellAt(x, y);
                if (current == null || !current.empty)
                {
                    continue;
                }
                ret.Add(current);
            }
            //Debug.Log(print);
            return ret;
        }
        public override bool Equals(object obj)
        {
            return gridCell.Equals(((SearchCell) obj).gridCell);
        }
    }
    private List<Vector3> FindPath(Vector3 targetPoint)
    {
        SimplePriorityQueue<SearchCell> open = new SimplePriorityQueue<SearchCell>();
        HashSet<Grid.Cell> closed = new HashSet<Grid.Cell>();

        SearchCell target = new SearchCell(0, 0, Grid.Instance.CellAtPoint(targetPoint.x, targetPoint.y), null);
        SearchCell start = new SearchCell(0, (targetPoint - transform.position).sqrMagnitude, Grid.Instance.CellAtPoint(transform.position.x, transform.position.y), null);
        open.Enqueue(start, start.f());

        // Explore grid
        SearchCell current = null;
        SearchCell temp = null;
        while (open.Count > 0)
        {
            current = open.Dequeue();
            closed.Add(current.gridCell);

            if (current.Equals(target))
                // Solution found
                break;
            
            foreach (Grid.Cell neighbour in current.GetNeighbours())
            {
                if (closed.Contains(neighbour)) continue;
                temp = new SearchCell(current.g+1, OctileDistance(current.gridCell.center, targetPoint), neighbour, current);
                if (open.Contains(temp))
                {
                    // Update g and parent
                    open.Remove(temp);
                }
                // Add to open list
                open.Enqueue(temp, temp.f());
            }
        }

        // Construct path back from current
        List<Vector3> path = new List<Vector3>();
        if (!current.gridCell.Equals(target.gridCell))
        {
            // Path couldn't be found
            Debug.Log(unitName + " couldn't find a path to " + targetPoint);
            return path;
        }
        //string pathStr = "Path found: ";
        while (current.parent != null)
        {
            //pathStr += current.gridCell.center;
            path.Insert(0, current.gridCell.center);
            current = current.parent;
        }
        //Debug.Log(pathStr);
        return path;
    }

}
